﻿
namespace HelloProgram.Shell.Services.Interfaces
{
    public interface IMessageService
    {
        string GetMessage();
    }
}
