﻿using HelloProgram.Shell.Services.Interfaces;

namespace HelloProgram.Shell.Services
{
    public class MessageService : IMessageService
    {
        public string GetMessage()
        {
            return "Hello from the Message Service";
        }
    }
}
