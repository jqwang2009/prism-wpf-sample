﻿using HelloProgram.Shell.Modules.ModuleName;
using HelloProgram.Shell.Services;
using HelloProgram.Shell.Services.Interfaces;
using HelloProgram.Shell.Views;
using Prism.Ioc;
using Prism.Modularity;
using System.Windows;

namespace HelloProgram.Shell
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IMessageService, MessageService>();
        }

        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            moduleCatalog.AddModule<ModuleNameModule>();
        }
    }
}
